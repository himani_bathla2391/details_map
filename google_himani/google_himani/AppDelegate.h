//
//  AppDelegate.h
//  google_himani
//
//  Created by CLI112 on 11/4/15.
//  Copyright (c) 2015 CLI112. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

