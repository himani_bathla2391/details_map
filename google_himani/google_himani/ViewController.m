//
//  ViewController.m
//  google_himani
//
//  Created by CLI112 on 11/4/15.
//  Copyright (c) 2015 CLI112. All rights reserved.
//

#import "ViewController.h"
#import "ViewController2.h"
CLLocationManager *manager;
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *googleMapLabel;
@property (strong, nonatomic) IBOutlet GMSMapView *googlemap;
@property (strong, nonatomic) IBOutlet UIButton *getDetailsLabel;

@property (weak, nonatomic) IBOutlet UILabel *latlabel;
@property (weak, nonatomic) IBOutlet UILabel *lonlabel;
@property (weak, nonatomic) IBOutlet UIImageView *bg1;

@end

@implementation ViewController
@synthesize googleMapLabel;
@synthesize googlemap;
@synthesize getDetailsLabel;
@synthesize latlabel;
@synthesize lonlabel;
@synthesize bg1;
CLLocationManager *manager;
NSString *country;
ViewController2 *second;
NSInteger buttontag;

CLLocationCoordinate2D position;
NSMutableArray *city;
NSMutableArray *destdata;
NSMutableArray *address;
CLPlacemark *placemark;
NSString *name;
NSDictionary *addressDictionary;
NSString *state;
NSString *cityName;
NSString *citya;
NSString *showaddress;
GMSMarker *marker ;



- (void)viewDidLoad {
    [super viewDidLoad];
    //[self getCurrentLocation];;
    
    city=[[NSMutableArray alloc]init];
    googlemap.delegate= self;
   // destdata=[[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view, typically from a nib.
    
}
//-(void) getCurrentLocation
//{
//    manager = [[CLLocationManager alloc]init];
//    manager.delegate = self;
//    manager.desiredAccuracy = kCLLocationAccuracyBest;
//    [manager requestWhenInUseAuthorization];
//    [manager startUpdatingLocation];
//    
//
//}




- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    CLLocation *currentLocation = newLocation;
    currentLocation = newLocation;
    
    if (currentLocation != 0)
        
    {
        
        position = currentLocation.coordinate;
        marker = [GMSMarker markerWithPosition:currentLocation.coordinate];
            marker.title = @"current loation";
            marker.map = googlemap;
            marker.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
            //[manager stopUpdatingLocation ] ;
        
        [city addObject:[NSString stringWithFormat:@"%.4f",currentLocation.coordinate.longitude]];
        
        [city addObject:[NSString stringWithFormat:@"%.4f",currentLocation.coordinate.latitude]];
        
        CLGeocoder *reverseGeocoder = [[CLGeocoder alloc] init];
        
        
        
        [reverseGeocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error)
         
         {
             
             
             
             CLPlacemark *myPlacemark = [placemarks objectAtIndex:0];
             
             NSString *countryCode = myPlacemark.ISOcountryCode;
             
             NSString *countryName = myPlacemark.country;
             
             NSString *cityName= myPlacemark.subAdministrativeArea;
             
             [city addObject:[NSString stringWithFormat:@"%@",cityName]];
             
             
             
             NSString *stateName= myPlacemark.administrativeArea;
             
             [city addObject:[NSString stringWithFormat:@"%@",stateName]];
             
             
             
             NSString *area= myPlacemark.country;
             
             [city addObject:[NSString stringWithFormat:@"%@",area]];
             
             
             
             NSLog(@"My country code:" @"%@" @"and countryName:" @"%@" @"MyCity:" @"%@", countryCode, countryName, cityName);
             
             NSDictionary *addressDictionary = myPlacemark.addressDictionary;
             
             NSString *show= addressDictionary[@"FormattedAddressLines"];
           [city addObject:show];
            NSLog(@"%@",show);
             
             
             
         }];
        
    }
    }

- (IBAction)getDetails:(id)sender {
    buttontag = 1;
    [self performSegueWithIdentifier:@"pass" sender:nil];
    
    
}

-(NSString *) getlocation: (CLLocationCoordinate2D )newLocation{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    CLLocation *nlocation = [[CLLocation alloc]initWithLatitude:newLocation.latitude
                                                      longitude:newLocation.longitude];
    
    [geocoder reverseGeocodeLocation :nlocation
                    completionHandler:^(NSArray *placemarks, NSError *error)
     {
         
         if (error) {
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         
         if (placemarks && placemarks.count > 0)
         {
             
             CLPlacemark *placemark = placemarks[0];
             
             //json parsing
             NSDictionary *addressDictionary =
             placemark.addressDictionary;
             NSLog(@"%@",addressDictionary);
             
            
             NSString *country= placemark.country;
             NSLog(@"%@",country);
             [city addObject:[NSString stringWithFormat:@"%@",country]];
             
             NSString *countrycode= placemark.ISOcountryCode;
             NSLog(@"%@",countrycode);
             [city addObject:[NSString stringWithFormat:@"%@",countrycode]];
             
             
            NSString *stateName= placemark.administrativeArea;
             NSLog(@"%@",stateName);
             [city addObject:[NSString stringWithFormat:@"%@",stateName]];
             
             NSString *cityName= placemark.locality;
             NSLog(@"%@",cityName);
            [city addObject:[NSString stringWithFormat:@"%@",cityName]];
             
             NSString *latitude= [NSString stringWithFormat:@"%f",newLocation.latitude];
              [city addObject:[NSString stringWithFormat:@"%@",latitude]];
             
             NSString *longitude= [NSString stringWithFormat:@"%f",newLocation.longitude];
              [city addObject:[NSString stringWithFormat:@"%@",longitude]];
             
            NSArray *show= addressDictionary[@"FormattedAddressLines"];
           showaddress = [show componentsJoinedByString:@"," ] ;
           NSLog(@"%@",showaddress);
           [city addObject:[NSString stringWithFormat:@"%@",showaddress]];
             
             //setting marker
             GMSMarker *markerall = [GMSMarker markerWithPosition:newLocation];
             markerall.title = showaddress;
             markerall.map = googlemap;
             markerall.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
         }
     }];
    return showaddress;
}


- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    
    
    [self getlocation:coordinate];
    [googlemap clear];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"pass"]){
        
    
    ViewController2 *vc = [segue destinationViewController];
    
    // Pass any objects to the view controller here, like...
    vc.tag= buttontag;
    ViewController2 *dest =segue.destinationViewController;
    dest.destdata=city;
    
    }
        }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
