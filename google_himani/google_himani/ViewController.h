//
//  ViewController.h
//  google_himani
//
//  Created by CLI112 on 11/4/15.
//  Copyright (c) 2015 CLI112. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import <AddressBook/AddressBook.h>

@interface ViewController : UIViewController<CLLocationManagerDelegate,GMSMapViewDelegate>
@property (nonatomic, assign) NSInteger tag;

@end

