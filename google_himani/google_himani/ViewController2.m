//
//  ViewController2.m
//  google_himani
//
//  Created by CLI112 on 11/4/15.
//  Copyright (c) 2015 CLI112. All rights reserved.
//

#import "ViewController2.h"

@interface ViewController2 ()
@property (weak, nonatomic) IBOutlet UIImageView *w1;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *stateLabel;
@property (strong, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *latlabel;
@property (weak, nonatomic) IBOutlet UILabel *countrylabel;
@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (strong, nonatomic) IBOutlet UILabel *lonLabel;
@property (strong, nonatomic) IBOutlet UILabel *formattedAdressLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryco;
@property (weak, nonatomic) IBOutlet UILabel *countrycodelabel;

@end

@implementation ViewController2
@synthesize countrylabel;
@synthesize tag;
@synthesize w1;
@synthesize nameLabel;
@synthesize stateLabel;
@synthesize cityLabel;
@synthesize latlabel;
@synthesize lonLabel;
@synthesize formattedAdressLabel;
@synthesize background;
@synthesize destdata;
@synthesize countryco;
@synthesize countrycodelabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    tag=1;
    // Do any additional setup after loading the view.
    
    //round image
    w1.layer.cornerRadius=70/2;
    w1.clipsToBounds= YES;
    
    NSString *str=[destdata objectAtIndex:0];
    NSLog(@"%@",str);
    if ([str isEqualToString:@"(null)"])
    {
         nameLabel.text=[NSString stringWithFormat:@"%@",str];
    }
    else
    {
         nameLabel.text=[NSString stringWithFormat:@"%@",str];
    }
    
   
    NSString *str1=[destdata objectAtIndex:1];
    NSLog(@"%@",str1);
    if ([str1 isEqualToString:@"IN"])
    {
        countrycodelabel.text=[NSString stringWithFormat:@"%@",str1];
        countrylabel.text=@"In India";
       [w1 setImage:[UIImage imageNamed:@"images.png"]];
        
    }
    else
    {
        countrycodelabel.text=[NSString stringWithFormat:@"%@",str1];
        countrylabel.text=@"Outside India";
        [w1 setImage:[UIImage imageNamed:@"i.jpg"]];
    }
    
    
  
    NSString *str2=[destdata objectAtIndex:2];
    NSLog(@"%@",str2);
    if ([str2 isEqualToString:@"(null)"])
    {
        stateLabel.text=[NSString stringWithFormat:@"NA"];
    }
    else
    {
        stateLabel.text=[NSString stringWithFormat:@"%@",str2];
    }
   
  
    NSString *str3=[destdata objectAtIndex:3];
    NSLog(@"%@",str3);
    if ([str3 isEqualToString:@"(null)"])
    {
        cityLabel.text=[NSString stringWithFormat:@"NA"];
    }
    else
    {
        cityLabel.text=[NSString stringWithFormat:@"%@",str3];
    }

    
    
    NSString *str4=[destdata objectAtIndex:4];
    NSLog(@"%@",str4);
    if ([str4 isEqualToString:@"(null)"])
    {
        latlabel.text=[NSString stringWithFormat:@"NA"];
    }
    else
    {
        latlabel.text=[NSString stringWithFormat:@"%@",str4];
    }

    
    NSString *str5=[destdata objectAtIndex:5];
    NSLog(@"%@",str5);
    if ([str5 isEqualToString:@"(null)"])
    {
        lonLabel.text=[NSString stringWithFormat:@"NA"];
    }
    else
    {
        lonLabel.text=[NSString stringWithFormat:@"%@",str5];
    }

   
    
    NSString *str6=[destdata objectAtIndex:6];
    NSLog(@"%@",str6);
    if ([str6 isEqualToString:@"(null)"])
    {
        formattedAdressLabel.text=[NSString stringWithFormat:@"NA"];
    }
    else
    {
        formattedAdressLabel.text=[NSString stringWithFormat:@"%@",str6];
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
